# amromics_ml

## genomedownload.py

Usage: 
```
python3 genomedownload.py --help
```

Example:
```
python3 genomedownload.py -g Citrobacter -d /Path/GenomeData/ -m /Path/Citrobacter-metafile.csv
```