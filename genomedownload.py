#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Usage

python3 genomedownload.py -d patric_data/
or
python3 genomedownload.py --help

"""

from __future__ import division, print_function, absolute_import


import wget
import urllib
import os
import pandas as pd
import numpy as np
import csv


def download_genome(genomeid, folder, filetype='PATRIC.ffn'):
    """
    Download data for a genome from PATRIC.
    :param genomeid (str): genome_id of the genome to download.
    :param folder (str): path to the folder to save downloaded genome data file.
    :param filetype (str): type of the file to download, default to 'PATRIC.ffn'.
    :return: If data file for the genomeid already exists, do nothing, return the genome id;
    If download successful, save the genome data file, return the genomeid;
    If download failed, print out notice, return None.
    """

    folder = folder.rstrip('/')
    filename = genomeid + '.' + filetype
    fullpathfile = os.path.join(folder, filename)
    if os.path.isfile(fullpathfile):
        return genomeid
    url = 'ftp://ftp.patricbrc.org/genomes/' + genomeid + '/' + filename
    try:
        print('\nDownloading file {}'.format(filename))
        wget.download(url, fullpathfile)
        return genomeid
    except BaseException as error:
        print(str(error))


def metafile_process(file):
    '''
    Download data from RELEASE_NOTES on Patric, process and put data into a pandas dataframe
    :param file (str): can be genome_lineage, genome_metadata, PATRIC_genomes_AMR.txt
    :return: pandas dataframe of data
    '''

    url = 'ftp://ftp.patricbrc.org/RELEASE_NOTES/' + file
    webdata = urllib.request.urlopen(url)
    header = webdata.readline().decode("utf-8").rstrip('\r\n').split('\t')
    data = []
    for line in webdata:
        data.append(line.decode("utf-8").rstrip('\r\n').split('\t'))
    return pd.DataFrame(data, columns=header).drop_duplicates(keep='first', inplace=False)


def download_genus(genus, datafolder, metafile, filetype='PATRIC.ffn'):
    '''
    Download data for all genomes belonging to a genus from PATRIC to datafolder and
    download their metadata to metafile.
    Metadata extracted from genome_lineage, genome_metadata, and PATRIC_genomes_AMR.txt from
    ftp://ftp.patricbrc.org/RELEASE_NOTES/
    :param genus (str)
    :param datafolder (str): path to the folder to save downloaded genome data files.
    :param metafile (str): file to save metadata.
    :param filetype (str): type of the files to download, default to 'PATRIC.ffn'.
    '''

    # EXTRACT SET OF GENOME_IDS TO DOWNLOAD
    # Get genome_ids belonging to the specified genus from genome_lineage
    lineage_df = metafile_process('genome_lineage')
    genomeingenus_df = lineage_df[lineage_df.genus.apply(lambda x: str(x).lower().strip() == genus.lower().strip())].\
                        loc[:, ['genome_id']]
    genomeingenus_df.drop_duplicates(keep='first', inplace=True)  # Retain only one copy for any duplicates

    # Retain only genome_ids tested with the 10 most common antibiotic for the genus in PATRIC_genomes_AMR
    amr_df = metafile_process('PATRIC_genomes_AMR.txt')
    amr_df.drop_duplicates(subset=['genome_id', 'antibiotic'], keep=False, inplace=True)  # Some genome_ids have
                                                 # different records for the same antibiotic => Remove these cases
    genomeantibiotic_df = amr_df.loc[:, ['genome_id', 'antibiotic', 'resistant_phenotype',\
                                         'measurement', 'measurement_unit']].\
                                merge(genomeingenus_df, how='right', on='genome_id')
    topantibiotic = genomeantibiotic_df.groupby('antibiotic').count()['genome_id'].\
                        sort_values(ascending=False)[:10].index
    genometopantibiotic_df = genomeantibiotic_df[genomeantibiotic_df.antibiotic.isin(topantibiotic)]

    # Get genome_ids with genome_status = WGS or Complete in genome_metadata
    metadata_df = metafile_process('genome_metadata')
    genomecomplete_df = metadata_df[metadata_df.genome_status.isin(['Complete', 'WGS'])].\
                        loc[:, ['genome_id', 'sequencing_platform', 'isolation_site', 'isolation_source',\
                                 'collection_date', 'latitude', 'longitude', 'altitude', 'host_name',\
                                 'host_gender', 'host_age', 'host_health', 'body_sample_site', 'body_sample_subsite']]

    # Merge all the data frames to get the final genome_id list
    genomeset = set(genometopantibiotic_df.merge(genomecomplete_df, how='left', on='genome_id')['genome_id'])

    # DOWNLOAD GENOME DATA
    if len(genomeset) == 0:
        print('No genome_id found for genus: ' + genus)
        return

    genomedownloadedset = set()
    for genomeid in genomeset:
        genomedownloadedid = download_genome(genomeid, datafolder, filetype)
        if genomedownloadedid is None:
            continue
        genomedownloadedset.add(genomedownloadedid)

    # PRINT OUT METADATA
    if len(genomedownloadedset) == 0:
        print('No genome data file downloaded.')
        return

    genomedownloadedamr_df = genometopantibiotic_df[genometopantibiotic_df.genome_id.isin(genomedownloadedset)]
    genomedownloadedamr_df.resistant_phenotype[genomedownloadedamr_df.resistant_phenotype == ''] =\
        'measurement ' + genomedownloadedamr_df.measurement + ' ' + genomedownloadedamr_df.measurement_unit
    resistantphenotypepivot_df = genomedownloadedamr_df.set_index(['genome_id']).\
        pivot(columns="antibiotic")['resistant_phenotype'].reset_index().rename_axis(None, axis=1)

    meta_df = resistantphenotypepivot_df.merge(genomecomplete_df, how='left', on='genome_id')
    meta_df.to_csv(metafile, sep=',', quoting=csv.QUOTE_NONNUMERIC, index=False)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Download.')

    parser.add_argument('-g', '--genus',  help='Genus to download',  default='Salmonella', type=str)
    parser.add_argument('-m', '--meta_file', help='Meta data file', default='metadata.txt')
    parser.add_argument('-d', '--data_folder', help='Folder to store data', default='.')

    args = parser.parse_args()
    download_genus(args.genus, args.data_folder, args.meta_file, filetype='PATRIC.ffn')


if __name__ == '__main__':
    main()

